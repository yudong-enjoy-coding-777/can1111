/**
  ******************************************************************************
  * @file    can.c
  * @brief   This file provides code for the configuration
  *          of the CAN instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

CAN_HandleTypeDef hcan1;

/* CAN1 init function */
void MX_CAN1_Init(void)
{

  /* USER CODE BEGIN CAN1_Init 0 */

  /* USER CODE END CAN1_Init 0 */

  /* USER CODE BEGIN CAN1_Init 1 */

  /* USER CODE END CAN1_Init 1 */
  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 3;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_9TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_4TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN1_Init 2 */

  /* USER CODE END CAN1_Init 2 */

}

void HAL_CAN_MspInit(CAN_HandleTypeDef* canHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspInit 0 */

  /* USER CODE END CAN1_MspInit 0 */
    /* CAN1 clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* CAN1 interrupt Init */
    HAL_NVIC_SetPriority(CAN1_TX_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_TX_IRQn);
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
    HAL_NVIC_SetPriority(CAN1_RX1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);
    HAL_NVIC_SetPriority(CAN1_SCE_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_SCE_IRQn);
  /* USER CODE BEGIN CAN1_MspInit 1 */

  /* USER CODE END CAN1_MspInit 1 */
  }
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* canHandle)
{

  if(canHandle->Instance==CAN1)
  {
  /* USER CODE BEGIN CAN1_MspDeInit 0 */

  /* USER CODE END CAN1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_CAN1_CLK_DISABLE();

    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);

    /* CAN1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN1_TX_IRQn);
    HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
    HAL_NVIC_DisableIRQ(CAN1_RX1_IRQn);
    HAL_NVIC_DisableIRQ(CAN1_SCE_IRQn);
  /* USER CODE BEGIN CAN1_MspDeInit 1 */

  /* USER CODE END CAN1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */

//这里要自己配过滤�??
void CAN1_Filter_Init(void)
{
	CAN_FilterTypeDef CAN1_FilterConf;
	CAN1_FilterConf.FilterIdHigh=0X0000;
	CAN1_FilterConf.FilterIdLow=0X0000;  //set id for filter
	CAN1_FilterConf.FilterMaskIdHigh=0X0000;
	CAN1_FilterConf.FilterMaskIdLow=0X0000;  //set mask_id for filter
	CAN1_FilterConf.FilterFIFOAssignment=CAN_FILTER_FIFO0;  //设置FIFO与过滤器的关联关�??
	CAN1_FilterConf.FilterActivation=ENABLE;  //�??活过滤器
	CAN1_FilterConf.FilterMode=CAN_FILTERMODE_IDMASK;  //设置模式
	CAN1_FilterConf.FilterScale=CAN_FILTERSCALE_32BIT;  //设置过滤器的位宽�??2*16 or 1*32�??
	CAN1_FilterConf.FilterBank=0;  //设置初始化的过滤器组

	if(HAL_CAN_ConfigFilter(&hcan1, &CAN1_FilterConf)!=HAL_OK)
	{
		printf("CAN Filter Config Fail!\r\n");
		Error_Handler();
	}

	printf("CAN Filter Config Success!\r\n");
}

//发�?�标准ID的数据帧
uint8_t CANx_SendStdData(CAN_HandleTypeDef* hcan,uint16_t ID,uint8_t *pData,uint16_t Len)
{
  static CAN_TxHeaderTypeDef   Tx_Header;

	Tx_Header.StdId=ID;
	Tx_Header.ExtId=0;
	Tx_Header.IDE=CAN_ID_STD;
	Tx_Header.RTR=0;
	Tx_Header.DLC=Len;
        /*找到空的发�?�邮箱，把数据发送出�??*/
    /*
	if(HAL_CAN_AddTxMessage(hcan, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX0) != HAL_OK) //
	{
		if(HAL_CAN_AddTxMessage(hcan, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX1) != HAL_OK)
		{
			HAL_CAN_AddTxMessage(hcan, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX2);
		}
	}
	*/
	if (HAL_CAN_AddTxMessage(&hcan1, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX0) != HAL_OK) {
        printf("CAN send test data fail!\r\n");
        Error_Handler();
    }

	printf("CAN send data success!\r\n");
}

//发�?�扩展ID的数据帧
uint8_t CANx_SendExtData(CAN_HandleTypeDef* hcan,uint32_t ID,uint8_t *pData,uint16_t Len)
{
	static CAN_TxHeaderTypeDef   Tx_Header;

	Tx_Header.RTR=0;
	Tx_Header.DLC=Len;
	Tx_Header.StdId=0;
	Tx_Header.ExtId=ID;
	Tx_Header.IDE=CAN_ID_EXT;
       /*找到空的发�?�邮箱，把数据发送出�??*/
	/*
	if(HAL_CAN_AddTxMessage(&hcan, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX0) != HAL_OK) //
	{
		if(HAL_CAN_AddTxMessage(&hcan, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX1) != HAL_OK)
		{
			HAL_CAN_AddTxMessage(&hcan, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX2);
		}
	}*/
	if (HAL_CAN_AddTxMessage(&hcan1, &Tx_Header, pData, (uint32_t*)CAN_TX_MAILBOX0) != HAL_OK) {
        printf("CAN send test data fail!\r\n");
        Error_Handler();
    }
	printf("CAN send data success!\r\n");
}

/** 数据的接�??
 * @brief  CAN FIFO0的中断回调函数，在里面完成数据的接收
 * @param  hcan     CAN的句�??
 */

uint8_t date_CAN1[8];//设为全局变量，用于接收CAN1数据
uint8_t date_CAN2[8];//设为全局变量，用于接收CAN2数据

//CAN接收中断处理函数
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	if(hcan->Instance ==CAN1)
	{
	  CAN_RxHeaderTypeDef RxHeader;  //接受句柄
	  HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &RxHeader, date_CAN1); //接收，CAN邮箱�??0
	  //    各参数的含义�??    can1  FIFO0(接收邮箱0�??  接收容器   用于接收数据

	  printf("--->Data Receieve!\r\n");
	  printf("RxMessage.StdId is %#x\r\n",  RxHeader.StdId);
	  printf("data[0] is 0x%02x\r\n", date_CAN1[0]);
	  printf("data[1] is 0x%02x\r\n", date_CAN1[1]);
	  printf("data[2] is 0x%02x\r\n", date_CAN1[2]);
	  printf("data[3] is 0x%02x\r\n", date_CAN1[3]);
	  printf("data[4] is 0x%02x\r\n", date_CAN1[4]);
	  printf("data[5] is 0x%02x\r\n", date_CAN1[5]);
	  printf("data[6] is 0x%02x\r\n", date_CAN1[6]);
	  printf("data[7] is 0x%02x\r\n", date_CAN1[7]);
	  printf("<---\r\n");

	  return ;
	}

}



/*
uint8_t CAN1_Send_Msg(CANTxMsg_t *msg,uint16_t mailbox_id,uint8_t *sendbuff)
{
	uint8_t id;
	msg->TxMessage.StdId=mailbox_id; //ID�??
	msg->TxMessage.IDE=CAN_ID_STD;
	msg->TxMessage.DLC=8;
	msg->TxMessage.RTR=CAN_RTR_DATA;
	msg->TxMessage.TransimtGlobalTime=DISABLE;

	for(id=0;id<8;id++)
	{
		msg->payload[id]=sendbuff[id];
	}

	if(HAL_CAN_AddTxMessage(&hcan1, &msg->TxMessage, msg->payload, &msg->mailbox)!=HAL_OK)
	}
*/

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
